#!/usr/bin/env python
#
# jhfarmclient.py
#
# (c) 2005, Davyd Madeley
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#  
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#  
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# This is the JHBuildfarm client, an XML-RPC client for getting jhbuild modules
# and building them. This should run pretty much anywhere, as long as it is
# using the same directories to checkout and install source as all the other
# client machines.

import xmlrpclib
import time, os

name = "farmclient-%i" % os.getpid ()

UNBUILT = "unbuilt"
BUILDING = "building"
BUILT = "built"
ERROR = "error"

server = xmlrpclib.ServerProxy ("http://localhost:8000")

server.connect (name)

build = server.get_build ()

while True:
	module = server.request (name)
	if module == "-1":
		print ' -- %s - queue is empty, ending' % name
		break;
	elif module == '':
		print ' -- %s - no modules available, waiting 30s' % name
		time.sleep (30)
		continue;
	
	print " -- %s - building module %s" % (name, module)
	server.report (name, module, BUILDING)

	# build the module
	state = module.STATE_START

	server.report (name, module, BUILT)
