#!/usr/bin/env python
#
# jhfarmerserver.py
#
# (c) 2005, Davyd Madeley
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#  
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#  
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# This is the JHBuildfarm Server, an XML-RPC server for handing out jhbuild
# jobs to client processes. This should run pretty much anywhere, as long
# as jhbuild is using the same directories to install and checkout on each
# machine.

import os, os.path

configfile = os.path.join (os.environ["HOME"], '.jhbuildrc')

import SimpleXMLRPCServer
import sys

sys.path.insert (0, '/home/davyd/src/CVS/jhbuild')

import jhbuild.config
import jhbuild.moduleset

UNBUILT = "unbuilt"
BUILDING = "building"
BUILT = "built"
ERROR = "error"

class FarmServer:
	def __init__ (self):
		self.build = None
		self.clients = {}
		self.queue = []
		self.queued = []
		self.modules = {}
	def enqueue (self, queue, modules):
		self.queue = queue
		self.modules = modules
		for module in queue:
			module.state = UNBUILT
	def dequeue (self):
		for module in self.queue:
			clear = True
			for dep in module.dependencies:
				if self.modules[dep].state != BUILT:
				    if self.modules[dep].state == FAILED:
					self.build.message ('module %s not built due to non buildable %s' % (module.name, dep))
				    clear = False
				    break;
			if clear:
				self.queue.remove (module)
				self.queued.append (module)
				return module.name

		# we did not find a module to build, check to see if any
		# possibly remain
		all_possible_complete = True
		for module in self.queued:
			if module.state == BUILDING:
				all_possible_complete = False
				break
		
		if all_possible_complete:
			print " -- no more buildable modules remaining"
			return "-1"
		else:
			# wait to see if more modules become available
			return ''

class FarmClient:
	def __init__ (self, name):
		self.name = name

class API:	
	def connect (self, name):
		print " -- got a new connection: %s" % name
		farm.clients[name] = FarmClient (name)
		return 0
	
	def request (self, name):
		print " -- got a request from %s for a new task" % name
		if len (farm.queue) > 0:
			module = farm.dequeue ()
			print " -- giving %s the module %s" % (name, module)
			return module
		else:
			print " -- telling %s to end" % name
			return "-1"

			# XXX: we need to be able to shut down the XML-RPC
			# server around here
	
	def report (self, name, module, state):
		print " -- got a report from %s on module %s: %s -> %s" % (name, module, farm.modules[module].state, state);
		farm.modules[module].state = state
		return ''

if __name__ == '__main__':
	server = SimpleXMLRPCServer.SimpleXMLRPCServer (("localhost", 8000))
	farm = FarmServer ()
	
	config = jhbuild.config.Config (configfile)
	print "Using module set: %s" % config.moduleset
	moduleset = jhbuild.moduleset.load (config)
	farm.enqueue (moduleset.get_module_list (config.modules, config.skip), moduleset.modules)
	print "%s modules in set" % len (farm.queue)
	farm.build = jhbuild.frontends.get_buildscript (config, moduleset)

	farm.build.start_build ()

	server.register_instance (API ())

	farm.build.end_build (None) # XXX failures

	try:
		print "Starting farm master, waiting for clients..."
		server.serve_forever ()
	except KeyboardInterrupt:
		print "Interrupted"
		server.socket.close ()
		print "  exiting..."
		sys.exit (1)
